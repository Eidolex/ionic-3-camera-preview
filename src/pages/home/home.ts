import { CameraPreview } from '@ionic-native/camera-preview/';
import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { OpenCvProvider } from '../../providers/open-cv/open-cv';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit {
  cameraTask: Subscription = undefined;
  imageList: string[] = [];
  faceCascade: any;
  constructor(
    public navCtrl: NavController,
    public toastCtrl: ToastController,
    private cameraPreview: CameraPreview,
    private openCvProvider: OpenCvProvider
  ) { }

  async ngOnInit(): Promise<void> {
    const load1 = this.openCvProvider.load();
    const load2 = this.openCvProvider.createFileFromUrl(
      `assets/opencv/haarcascade_frontalface_default.xml`
    );
    const result = await Promise.all([ load1, load2 ]);
    this.openCvProvider.setFaceCascadeFile(
      'haarcascade_frontalface_default.xml',
      result[ 1 ]
    );
    await this.open();
    this.startCameraTask();
  }

  /**
   *
   * @param {string} image base64 string
   */
  private checkPicture(image: string): Promise<boolean> {
    return new Promise((resolve) => {
      const img = new Image();
      img.onload = async () => {
        const result = await this.detectFace(img);
        resolve(result);
      };
      img.src = image;
    });
  }

  private async detectFace(image: HTMLImageElement): Promise<boolean> {
    try {
      const src = cv.imread(image);
      const gray = new cv.Mat();
      const msize = new cv.Size(0, 0);
      const faces = new cv.RectVector();
      const faceCascade = new cv.CascadeClassifier();
      cv.cvtColor(src, gray, cv.COLOR_RGBA2GRAY, 0);
      faceCascade.load('haarcascade_frontalface_default.xml');
      faceCascade.detectMultiScale(gray, faces, 1.1, 3, 0, msize, msize);

      const result = faces.size() > 0;
      src.delete();
      gray.delete();
      faces.delete();
      faceCascade.delete();
      return result;
    } catch (error) {
      console.log(error);
      return false;
    }
  }

  private async open() {
    try {
      let options = {
        x: 0,
        y: window.screen.height - 250,
        width: 250,
        height: 250,
        camera: this.cameraPreview.CAMERA_DIRECTION.FRONT,
        toBack: false,
        tapPhoto: true,
        tapFocus: false,
        previewDrag: false,
        storeToFile: false,
        disableExifHeaderStripping: false,
      };
      await this.cameraPreview.startCamera(options);
    } catch (error) {
      console.log(error);
    }
  }

  private startCameraTask() {
    if (!this.cameraTask) {
      let interval = Observable.interval(800);
      this.cameraTask = interval.subscribe(async () => {
        const image = await this.takePicture();
        if (image && (await this.checkPicture(image))) {
          if (this.imageList.length > 4) {
            this.stopCameraTask();
          }
          this.imageList.push(image);
          this.toastCtrl
            .create({ message: 'New image added', duration: 1500 })
            .present();
          if (this.imageList.length > 4) {
            this.stopCameraTask();
          }
        }
      });
    }
  }

  private stopCameraTask() {
    if (this.cameraTask) {
      this.cameraTask.unsubscribe();
      this.cameraPreview.stopCamera();
    }
  }

  private async takePicture(): Promise<string> {
    try {
      const res = await this.cameraPreview.takePicture({
        width: 320,
        height: 320,
        quality: 85,
      });
      return `data:image/jpeg;base64,${res}`;
    } catch (error) {
      console.log(error);
      return null;
    }
  }
}
