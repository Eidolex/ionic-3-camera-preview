import { Injectable } from '@angular/core';
/*
  Generated class for the OpenCvProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
export const ScriptStore: string = '../../../assets/js/opencv.js';

@Injectable()
export class OpenCvProvider {
  private loaded: boolean = false;
  constructor() {}

  public load(): Promise<void> {
    return new Promise((resolve) => {
      if (this.loaded) {
        resolve();
      }

      let script = document.createElement('script');
      script.type = 'text/javascript';
      script.src = ScriptStore;
      if (script.readyState) {
        //IE
        script.onreadystatechange = () => {
          if (
            script.readyState === 'loaded' ||
            script.readyState === 'complete'
          ) {
            script.onreadystatechange = null;
            cv['onRuntimeInitialized'] = () => {
              this.loaded = true;
              resolve();
            };
          }
        };
      } else {
        //Others
        script.onload = () => {
          cv['onRuntimeInitialized'] = () => {
            this.loaded = true;
            resolve();
          };
        };
      }
      script.onerror = (error: any) => resolve();
      document.getElementsByTagName('head')[0].appendChild(script);
    });
  }

  public createFileFromUrl(url): Promise<Uint8Array> {
    const request = new XMLHttpRequest();
    request.open('GET', url, true);
    request.responseType = 'arraybuffer';
    return new Promise((resolve, reject) => {
      request.onload = (ev) => {
        if (request.readyState === 4) {
          if (request.status === 200) {
            const data = new Uint8Array(request.response);
            resolve(data);
          } else {
            reject();
          }
        }
      };
      request.send();
    });
  }

  public setFaceCascadeFile(path: string, data: Uint8Array) {
    cv.FS_createDataFile('/', path, data, true, false, false);
  }
}
