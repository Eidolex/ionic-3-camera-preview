interface HTMLScriptElement {
  readyState: any;
  onreadystatechange: () => void;
}
